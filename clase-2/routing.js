const http = require('node:http') // protocolo HTTP
const fs = require('node:fs')
const desiredPort = process.env.PORT ?? 1234


const dittoJSON= require('./pokemon/ditto.json')

const processRequest = (req, res) => {

    

    const {method, url}= req
    console.log(req.url)
    switch (method){
        case 'GET':
            switch (url){
                case '/pokemon/ditto':{

                    res.setHeader('Content-Type', 'application/json; charset=utf-8')
                    return res.end(JSON.stringify(dittoJSON))
                }
                default :{
                    res.statusCode=404   
                    res.setHeader('Content-Type', 'text/html; charset=utf-8')
                    return res.end('<h1>404 not found<h1>')
                }
            }
        case 'POST':    

    }
}

const server = http.createServer(processRequest)

server.listen(desiredPort, () => {
    console.log(`server listening on port http://localhost:${desiredPort}`)
})
