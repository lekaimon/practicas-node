const http = require('node:http') // protocolo HTTP
const fs = require('node:fs')
const desiredPort = process.env.PORT ?? 1234




const processRequest = (req, res) => {

    res.setHeader('Content-Type', 'text/plain ; charset=utf-8')

    if (req.url === '/') {
        res.statusCode = 200 // ok
        res.end('Buena tardes á')
    } else if (req.url === '/imagen-random') {
        
        fs.readFile('../img/avatar4.png', (err,data) => {
            if (err) {
                res.statusCode = 500
                res.end('<h1>500 internal server error<h1>')
            }else{
                res.setHeader('Content-Type', 'image/png')
                res.end(data)
            }
        });
    }
}

const server = http.createServer(processRequest)

server.listen(desiredPort, () => {
    console.log(`server listening on port http://localhost:${desiredPort}`)
})
