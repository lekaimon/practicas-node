//modulos nativos

const os = require ('node:os');

console.log("info del SO");
console.log('-------------------------------');

console.log('Nombre',os.platform());
console.log('Version',os.release());
console.log('Arquitectura',os.arch());
console.log('CPUs',os.cpus());
console.log('Memoria libre',os.freemem/1024/1024 ,'MB');
console.log('Memoria total', os.totalmem /1024/1024 , 'MB');
