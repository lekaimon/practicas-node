const fs = require('node:fs');//node:fs desde la 16.0 es recomendable

const stats = fs.statSync('./archivo.txt');

console.log(

    stats.isFile(),
    stats.isDirectory(),
    stats.isSymbolicLink(),//enlace simbolico
    stats.size//tamaño en bytes


)